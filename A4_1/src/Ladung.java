
public class Ladung {
    private String type;
    private int menge;

    public Ladung() {

    }

    public Ladung(String type, int menge){
        setType(type);
        setMenge(menge);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getMenge() {
        return menge;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }

    @Override
    public String toString(){
        String string = "Ladung: "+type+" "+menge;
        return string;
    }
}
