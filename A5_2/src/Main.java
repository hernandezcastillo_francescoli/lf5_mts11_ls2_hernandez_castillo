import java.util.ArrayList;

/**@author FrancescoliAldairHernandezCastillo
 * @version 1
 */

public class Main {

    /** Klasse Mein erstellt
     * Ladungen und Raumschiff erstellen
     * Auszuführende Methoden in der Main
     */

    public static void main(String[] args) {
        Ladung ladung1 = new Ladung("Ferengi Schneckensaft", 200);
        Ladung ladung2 = new Ladung("Borg-Schrott", 5);
        Ladung ladung3 = new Ladung("Rote Materie", 2);
        Ladung ladung4 = new Ladung("Forschungssonde", 35);
        Ladung ladung5 = new Ladung("Bath'leth Klingonen Schwert", 200);
        Ladung ladung6 = new Ladung("Plasma-Waffe", 50);
        Ladung ladung7 = new Ladung("Photonentorpedo", 3);

        Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 100, 100, 100, 100, 1, 2);
        Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2);
        Raumschiff vulkanier = new Raumschiff("Ni'Var", 80, 80, 100, 50, 0, 5);

        klingonen.ladenLadungen(ladung1);
        klingonen.ladenLadungen(ladung5);
        romulaner.ladenLadungen(ladung2);
        romulaner.ladenLadungen(ladung3);
        romulaner.ladenLadungen(ladung6);
        vulkanier.ladenLadungen(ladung4);
        vulkanier.ladenLadungen(ladung7);

        klingonen.abschiessenPhotonentorpedos(romulaner);
        romulaner.abschiessenPhaserkanonen(klingonen);
        vulkanier.sendenNachricht("Gewalt ist nicht logisch");
        klingonen.getZustandRaumschiffes();
        klingonen.abschiessenPhotonentorpedos(romulaner);
        klingonen.abschiessenPhotonentorpedos(romulaner);
        klingonen.getZustandRaumschiffes();
        romulaner.getZustandRaumschiffes();
        vulkanier.getZustandRaumschiffes();
        ArrayList<String> broadcastKommunikator = klingonen.getLogbuch();
        System.out.println("Broadcast Kommunikator:");
        for(String message:broadcastKommunikator){
            System.out.println(message);
        }
    }
}