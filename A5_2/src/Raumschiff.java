import java.util.ArrayList;


/**@author FrancescoliAldairHernandezCastillo
 * @version 1
 */

/**Voraussetzung.: Klasse Raumschiff erstellt*/
public class Raumschiff {

    private String name;
    private int energieVersorgung;
    private int schutzSchilde;
    private int lebenserHaltungssysteme;
    private int hulle;
    private int photonenTorpedos;
    private int reparaturAndroiden;
    private ArrayList<Ladung> ladungsVerzeichnis = new ArrayList<>();
    private ArrayList<String> broadcastKommunikator = new ArrayList<>();
    private String nachricht;

    public Raumschiff() {

    }

    /** @param name name des Raumschiffs
     * @param energieVersorgung Ursprünglicher Energieversorgung
     * @param schutzSchilde Ursprünglicher Schutz Schilde
     * @param lebenserHaltungssysteme Ursprünglicher Lebenser Haltungssysteme
     * @param hulle Ursprünglicher Hulle
     * @param photonenTorpedos Ursprünglicher Photonen Torpedos
     * @param reparaturAndroiden Ursprünglicher Reparatur Androiden
     */
    public Raumschiff(String name, int energieVersorgung, int schutzSchilde, int lebenserHaltungssysteme, int hulle,
                      int photonenTorpedos, int reparaturAndroiden){
        setName(name);
        setEnergieVersorgung(energieVersorgung);
        setSchutzSchilde(schutzSchilde);
        setLebenserHaltungssysteme(lebenserHaltungssysteme);
        setHulle(hulle);
        setPhotonenTorpedos(photonenTorpedos);
        setReparaturAndroiden(reparaturAndroiden);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEnergieVersorgung() {
        return energieVersorgung;
    }

    public void setEnergieVersorgung(int energieVersorgung) {
        this.energieVersorgung = energieVersorgung;
    }

    public int getSchutzSchilde() {
        return schutzSchilde;
    }

    public void setSchutzSchilde(int schutzSchilde) {
        this.schutzSchilde = schutzSchilde;
    }

    public int getHulle() {
        return hulle;
    }

    public void setHulle(int hulle) {
        this.hulle = hulle;
    }

    public int getPhotonenTorpedos() {
        return photonenTorpedos;
    }

    public void setPhotonenTorpedos(int photonenTorpedos) {
        this.photonenTorpedos = photonenTorpedos;
    }

    public int getReparaturAndroiden() {
        return reparaturAndroiden;
    }

    public void setReparaturAndroiden(int reparaturAndroiden) {
        this.reparaturAndroiden = reparaturAndroiden;
    }

    public void getLadungsVerzeichnis() {
        System.out.println("-------------------------------------------------------\n\nRaumschiff: " + name +
                "\nLadung(en): " + ladungsVerzeichnis.size());
        int totalLadungen = ladungsVerzeichnis.size();

        for (int i = 0; i < totalLadungen; i++) {
            System.out.println("\nLadung: " + (totalLadungen + 1) + "\n-Bezeichnung: " + ladungsVerzeichnis.
                    get(i).getType() + "\n-Menge: " + ladungsVerzeichnis.get(i).getMenge());
        }
    }

    public void setLadungsVerzeichnis(ArrayList<Ladung> ladungsVerzeichnis) {
        this.ladungsVerzeichnis = ladungsVerzeichnis;
    }

    public void setBroadcastKommunikator(String broadcastKommunikator) {
        this.broadcastKommunikator.add(broadcastKommunikator);
    }

    public void setLebenserHaltungssysteme(int lebenserHaltungssysteme) {
        this.lebenserHaltungssysteme = lebenserHaltungssysteme;
    }

    /** Wenn es keine Torpedos gibt, ruf Metode sendenNachricht(nachricht: String) so wird als Nachricht an Alle -=*Click*=- ausgegeben.
     * Ansonsten wird die Torpedoanzahl um eins reduziert und die Nachricht an Alle Photonentorpedo abgeschossen gesendet.
     * Außerdem wird die Methode berechnenTreffer() aufgerufen.
     * @param ziel der Namen des feindlichen Schiffes
     */
    public void abschiessenPhotonentorpedos(Raumschiff ziel) {
        if (photonenTorpedos == 0) {
            sendenNachricht("-=*Click*=-");
        } else {
            photonenTorpedos--;
            sendenNachricht("Photonentorpedo abgeschossen");
            raumschiffGetroffe(ziel);
        }
    }

    /** Wenn die Energieversorgung kleiner als 50% ist, ruf Metode sendenNachricht(nachricht: String) so wird als
     * Nachricht an Alle -=*Click*=- ausgegeben.
     * Ansonsten wird die Energieversorgung um 50% reduziert und die Nachricht an Alle “Phaserkanone abgeschossen” gesendet.
     * Außerdem wird die Methode berechnenTreffer() aufgerufen.
     * @param ziel der Namen des feindlichen Schiffes
     */
    public void abschiessenPhaserkanonen(Raumschiff ziel) {
        if (energieVersorgung < 50) {
            sendenNachricht("-=*Click*=-");
        } else {
            energieVersorgung = 50;
            sendenNachricht("Phaserkanone abgeschossen");
            raumschiffGetroffe(ziel);
        }

    }

    /** Die Nachricht "[Schiffsname] wurde getroffen!" wird in der Konsole ausgegeben
     * @param ziel der Namen des feindlichen Schiffes
     */
    public void raumschiffGetroffe(Raumschiff ziel) {
        System.out.println(ziel.getName() + " wurde getroffen!");
        berechnenTreffer(ziel);
    }

    /** Schaden berechnet
     * @param ziel der Namen des feindlichen Schiffes
     */
    private void berechnenTreffer(Raumschiff ziel) {
        int tempSchilde = ziel.getSchutzSchilde();
        if (tempSchilde > 50)
            ziel.setSchutzSchilde(tempSchilde - 50);
        else if (tempSchilde > 0) {
            ziel.setSchutzSchilde(0);
        } else {
            if (ziel.getHulle() > 50){
                ziel.setHulle(ziel.getHulle()-50);
                ziel.setEnergieVersorgung(50);
            } else if(ziel.getHulle() >0) {
                ziel.setHulle(0);
            } else {
                ziel.setLebenserHaltungssysteme(0);
                sendenNachricht("Lebenserhaltungssysteme wurden zerstört");
            }
        }
    }

    /**
     * Die Nachricht wird dem broadcastKommunikator hinzugefügt
     * @param nachricht die neue Nachricht
     */
    public void sendenNachricht(String nachricht) {
        System.out.println(nachricht);
        setBroadcastKommunikator(nachricht);
    }

    /** Es hinzufugt eine neue Ladung
     * @param ladung neue Ladung
     */
    public void ladenLadungen(Ladung ladung) {
        ladungsVerzeichnis.add(ladung);
    }

    /** Addieren neuen Torpeden
     * @param torpedes neuen Torpeden
     */
    public void ladenPhotonentorpedos(int torpedes) {
        photonenTorpedos = photonenTorpedos + torpedes;
    }

    /** @return Gibt den broadcastKommunikator zurück.
     */
    public ArrayList<String> getLogbuch() {
        return broadcastKommunikator;
    }

    public void sendenReparaturauftrag(boolean schutzschilde, boolean energieversorgung, boolean schiffshulle) {

    }

    /** Es gibt alle Zustände (Attributwerte) des Raumschiffes auf der Konsole mit entsprechenden Namen aus.
     */
    public void getZustandRaumschiffes() {
        System.out.println("-------------------------------------------------------");
        System.out.println("Name: " + name + "\nEnergie Versorgung: " + energieVersorgung +
                "\nSchtz Schilde: " + schutzSchilde + "\nHulle: " + hulle +
                "\nPhotonen Torpedos: " + photonenTorpedos + "\nReparatur Androiden: " + reparaturAndroiden + "\n\n");
    }
}