/**@author FrancescoliAldairHernandezCastillo
 * @version 1
 */

/**Voraussetzung.: Klasse Ladung erstellt*/
public class Ladung {

    private String type;
    private int menge;

    /**@param type bezeichnung in der Ladung
     * @param menge mende in der Ladung
     */
    public Ladung() {

    }

    public Ladung(String type, int menge){
        setType(type);
        setMenge(menge);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getMenge() {
        return menge;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }

    /** @return der Name der Klasse und alle Zustände des Objekts liefern zurück
     */
    @Override
    public String toString(){
        String string = "Ladung: "+type+" "+menge;
        return string;
    }
}